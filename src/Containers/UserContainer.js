import { connect } from 'react-redux';
import { login, logout } from  '../redux/action';

import User from '../Components/User';

const mapStateToProps = state => ({
    state: state,
});

const mapDispatchToProps = dispatch => ({
    login: params=>dispatch(login(params)),
    logout: params=>dispatch(logout(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(User);