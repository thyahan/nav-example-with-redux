import initialState from './initial';

const user = (state=initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return Object.assign({}, state, { login: action.login });
        case 'LOGOUT':
            return Object.assign({}, state, { login: false });
        default:
            return state;
    }
}

export default user;