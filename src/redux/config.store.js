import { createStore } from 'redux'
import user from './reducers';

export default () => {

  const store = createStore(
    user
  )
  
  return store
}
