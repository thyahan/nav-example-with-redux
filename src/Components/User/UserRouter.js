import React, { Component } from 'react';

import { NavLink } from 'react-router-dom';
import { BrowserRouter, Router, Route } from 'react-router-dom';

const Login = props => {
    return <div><button onClick={props.onClick}>LOGIN</button></div>;
}

const Logout = props => {
    return <div><button onClick={props.onClick}>LOGOUT</button></div>;
}

class UserRouter extends Component {

    renderLogout(login=false){
        if(login !== false){
            return <li><NavLink to="/logout">logout</NavLink></li>;
        }
    }

    render() {
        console.log(this.props)
        return (
            <BrowserRouter>
                <div>
                    <ul>
                        <li><NavLink to="/login">Login</NavLink></li>
                        {this.renderLogout(this.props.state.login)}
                    </ul>
                <hr />
                {/* content */ }
                <Route path="/login" render={()=><Login onClick={this.props.login} />} />
                <Route path="/logout" render={()=><Logout onClick={this.props.logout} />} />
                </div>
            </BrowserRouter>
        );
    }
}

export default UserRouter;