import React, { Component } from 'react';
import configureStore from './redux/config.store';
import { Provider } from 'react-redux';
import UserContainer from './Containers/UserContainer';

const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store} key='provider'>	
        <UserContainer />
      </Provider>
    );
  }
}

export default App;
