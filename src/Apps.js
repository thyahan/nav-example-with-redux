import React, { Component } from 'react';

import configureStore from './redux/config.store';


import './App.css';
import { NavLink } from 'react-router-dom';
import { BrowserRouter, Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';


const store       = configureStore();

const login = () => {
  console.log(store.getState())
  return (
    <div>
      <p>make login</p>
    </div>
  );
}

const home = () => {
  return ( <div>home</div>)
}

class App extends Component {
  render() {
    return (
      <Provider store={store} key='provider'>	

      <BrowserRouter>
        
          <div>
            <ul>
              <li>
                <NavLink to="/login">Login</NavLink>
              </li>
              <li>
                <NavLink to="/">Home</NavLink>
              </li>
            </ul>
          <hr />

          <Route path="/" exact component={home} />
          <Route path="/login" component={login} />
          </div>
      </BrowserRouter>
      </Provider>

    );
  }
}

export default App;
